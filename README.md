Copyright &copy; 2020 Kirk Rader

# node-red-voice-assistant

Parent project with three submodules:

- [node-red-speech-client][]
- [node-red-speech-server][]
- [node-red-intent-automation][]

```mermaid
flowchart TD

  subgraph "Front End (one per room)"
    client[node-red-speech-client]
    microphone([Mic Array /<br/>Speaker])
  end

  subgraph "Back End (one per home)"
    server[node-red-speech-server]
    automation[node-red-intent-automation]
    broker[MQTT Broker]
  end

  subgraph "Local IoT Networks"
    devices[Various Devices]
  end

  client --> | 1. wait for wake phrase<br/>5. record utterance | client
  microphone --> | 2. wake phrase<br/>4. utterance | client
  client --> | 3. feedback sound<br/>6. feedback sound | microphone
  client --> | 7. recording | broker
  broker --> | 7. recording | server
  server --> | 8. Speech-to-Text<br/>9. Text-to-Intent | server
  server --> | 10. intent | broker
  broker --> | 10. intent | automation
  automation <--> | 11. monitoring and control | devices
  automation <--> | 12. dashboard | client

  classDef this text:black,fill:pink
  class client,server,automation this
```

> **Example front end:** Raspberry Pi in touch-screen enclosure, running [node-red-speech-client][], displaying dashboard from [node-red-intent-automation][] in "kiosk mode," with attached mic-array and speaker sitting on a console table in the living room:
>
> ![](./kiosk.jpg)
>
> This is connected via WiFi to another Raspberry Pi on which [node-red-intent-automation][] and the MQTT broker both run. Not shown is a third Raspberry Pi running [node-red-speech-server][] connected to the same WiFi network. Note that the front end and back end systems do not need to be in the same room. They do not theoretically even need to be in the same building if secure HTTP and MQTT connectivity is used to bridge multiple LAN's. (See <https://kirkrader.gitlab.io/docs/rpi-config/> for information on securing Node-RED and Mosquitto instances running on a Raspberry Pi exposed to the Internet.)

## Features

- Voice assistance without use of any third-party services such as those provided by our "friends" at Google, Amazon, Microsoft, Apple etc.
- Separates concerns between speech capture, intent recognition and automation
    - Distributes voice-processing load
    - Allows for multiple voice-capture locations in a single home
    - Shares configuration defining intents on a common server
    - Enables automation of heterogeneous device networks (Z-Wave, Zigbee, WiFi etc.) using agnostic protocols like MQTT and HTTP

## Rationale

Services like Amazon Alexa, Google Assistant, Apple Siri, Microsoft Cortana and their ilk all provide convenient functionality but at a significant cost in terms of performance, reliability, security and privacy.

These flows comprise a system to completely replace such services that, once set up, does not rely on an Internet connection at runtime. It does not share any of your private data with any third parties. It makes the functionality of your home automation system independent from the ability and willingness of commercial companies to keep their services running correctly at all times.

[node-red-speech-client]: https://gitlab.com/parasaurolophus/node-red-speech-client
[node-red-speech-server]: https://gitlab.com/parasaurolophus/node-red-speech-server
[node-red-intent-automation]: https://gitlab.com/parasaurolophus/node-red-intent-automation
